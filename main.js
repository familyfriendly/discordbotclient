let ipc = require("electron").ipcRenderer
let remote = require("electron").remote
let currentchannel;
let client  = remote.getGlobal("client")
const markdown = require( "markdown" ).markdown;
const fs = require("fs")





/*
LOAD CSS AND JS MODULES

*/
let paths = ["./modules/css","./modules/js"]

for(let path of paths) {
    fs.readdirSync(path).map((f) => {
        if(f.endsWith(".css")) {
            $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: `${"."+path}/${f}`
             }).appendTo("head");
        }
        else {
            let scr = require(`${"."+path}/${f}`)
            scr.load()
        }
    })
}

$(document).ready(function(){
    $('.modal').modal();
    $("#u-img").attr("src",client.user.avatarURL )
    $(`#${client.user.presence.status}`).addClass("selected")

  });

let ready = () => {
client.guilds.forEach((i) => {
    $("#server-sidebar").append(`<img href="#" id="${i.id}" onclick="window.loadServer(this)" src="https://cdn.discordapp.com/icons/${i.id}/${i.icon}.webp?size=128"> </img>`)
})
}

let newMsg = (author_name,content,attatchments) => {
    let attatcharr = attatchments.array()

let message=` 
<li> 
<b>${author_name}:</b>${markdown.toHTML(content)}
</li>
${attatcharr ? attatcharr.map(i => {
    return `
    <li>
    <img style="width:50%;" src="${i.url}" />
    </li>
    `
}).join("<br>") : null}
`
console.log(message)
$("#chat").append(message)


}

ready()

client.on("message",(message) => {
    
    if(currentchannel && currentchannel.id === message.channel.id) {
        newMsg(`${message.author.username}#${message.author.discriminator}`,message.content,message.attachments)

        $("#chat").animate({ scrollTop: $('#chat').prop("scrollHeight")}, 1000);
    }
        else if(window.settings.main.general.muteToasts === "false") M.toast({html:`<a id="${message.channel.id}" style="color:white;text-decoration: none; background-color: none;"href="#" onclick="window.toggleChannel(this)"><b>${message.author.username}/${message.channel.name}</b>: ${message.content} </a>`})
})





window.toggleChannel = (that) => {
    let channel = client.channels.get($(that).attr("id"))
    $("#name").text(channel.name)
    $("#desc").text(channel.topic||"no channel topic")
    currentchannel = channel
    $("#chat").empty()
    let fetched = channel.fetchMessages({ limit: 100 })
    .then(messages => {
        let messagearray = messages.array()
        for(var i = messagearray.length; i--;) {
            let message = messagearray[i]
            newMsg(`${message.author.username}#${message.author.discriminator}`,message.content,message.attachments)
        }

    })
    
}

window.loadServer = (that) => {
    let carr = client.guilds.get($(that).attr("id")).channels.array()
    let categories = carr.filter((c) => c.type === "category")
    let textchannels = carr.filter((c) => c.type === "text")
    $("#channel-sidebar").empty()
    for(let channel of categories) {
        $("#channel-sidebar").append(`<div class="content-master>"><h5>${channel.name}</h5><div id="${channel.id}" class="content-children"></div></div>`)
    }
    for(let channel of textchannels) {
       $(`#${channel.parentID}`).append(`<a id="${channel.id}" onclick="window.toggleChannel(this)" href="#">${channel.name}</a><br>`)
    }
}

window.sendMessage = () => {
    var myFile = $('#avatar').prop('files');
    var embed = $('#json').val();
    if($("#textarea1").val().length > 2000) return M.toast({html:`message exceeds 2000 chars (${$("#textarea1").val().length - 2000} too much)`})
    embed ? embed =  JSON.parse(embed) : null
    console.log(embed)
    currentchannel.send($("#textarea1").val(), {
         files:  myFile[0] ? [{
            attachment: myFile[0].path,
            name: myFile[0].name 
          }] : null,
          embed: embed ? 
          embed
            : null
    })
        .catch(err => M.toast({html:`${err}`}))
    
    
    $("#textarea1").val("")
    $('#avatar').val("")
    $('#json').val("");
}




window.changeStatus = (status) => {
    $(`#${client.user.presence.status}`).removeClass("selected")
    client.user.setStatus(status)
    .catch((err) => {M.toast({html:err})})
    $(`#${client.user.presence.status}`).addClass("selected")
}