const EventEmitter = require('events')
const eventEmitter = new EventEmitter()
const { app, BrowserWindow, ipcMain } = require('electron')
const Discord = require("discord.js")
global.client = new Discord.Client()

let mainWindow
function createWindow() {
    mainWindow = new BrowserWindow({
        width: 640,
        height: 820,
        webPreferences: {
            nodeIntegration: true
          }
    })
    mainWindow.loadFile(`${__dirname}/html/login.html`)
    mainWindow.on('closed', () => mainWindow = null)
    
}

console.log(`${__dirname}/html/login.html`)
app.on('ready', createWindow)
app.on('window-all-closed', () => process.platform == 'darwin' ? null :app.quit())
app.on('activate', () => mainWindow === null ? createWindow() : null)

ipcMain.on("load", (e,obj) => {
mainWindow.loadFile(`${__dirname}/html/${obj.page}.html`)
})

ipcMain.on("login", (e,token) => {
    global.client.login(token)
        .catch(err=> {
            return mainWindow.webContents.send('loginres', {msg:err.message})
        })
    })






    global.client.on("ready",() => {
    mainWindow.webContents.send('loginres', {msg:`logged in as ${client.user.username}; redirecting...`})
    mainWindow.loadFile(`${__dirname}/html/main.html`)
})

