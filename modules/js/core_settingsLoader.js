/*
THIS IS A CORE MODULE! DO NOT REMOVE OR ALTER
*/

exports.load = () => {
 window.settings = {}
fs.readdirSync("./modules/settings").map((f) => {
    let FileJson = JSON.parse(fs.readFileSync(`./modules/settings/${f}`))
    window.settings[f.split(".")[0]] = FileJson;
})

}