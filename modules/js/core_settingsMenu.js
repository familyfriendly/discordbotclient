
/*
THIS IS A CORE MODULE! DO NOT REMOVE OR ALTER
*/



const fs = require("fs")
let jsonfiles = {};
exports.load = () => {

}

window.openSettingsMenu = () => {
$("#overlay").fadeIn(200)
loadSettings()
}


window.dismissSettings = () => {
$("#overlay").fadeOut()
}

let loadSettings = () => {
    $("#settings-list").empty()

    fs.readdirSync("./modules/settings").map((f) => {
        let FileJson = JSON.parse(fs.readFileSync(`./modules/settings/${f}`))
        jsonfiles[f] = FileJson;

            
  

        
        $("#settings-list").append(
            `
            <li>
            <h5>${f}</h5>
            </li>
            `)  
            SettingsNesting(FileJson,f)
    })
}

let nest = (obj,f) => {
if(!obj.path) return {
    children:obj,
    file:f,
    path:``
} 
else return obj
}

let SettingsNesting = (obj,f) => {
    obj = nest(obj,f)

    for (var k in obj.children)
    {   
        if (typeof obj.children[k] == "object" && obj[k] !== null)
       { 
        SettingsNesting({
            children:obj.children[k],
            file: obj.file,
            path:`${obj.path ? obj.path+"/" : ""}${k}`
        });
        }
        else nestedAppend({
            value:obj.children[k],
            last:k,
            file:obj.file,
            path:`${obj.path}/`
        })
            // do something... 
    }

}



let nestedAppend = (obj) => {
    $("#settings-list").append(
        `
        <li>
        <h6 class="nestedParent">${obj.path}</h6><a href="#" onclick="window.loadSetting('${obj.file}','${obj.path !== "/"? obj.path : ""}${obj.last}','${obj.value}')">${obj.last}</a>
        </li>
        `)  

}


let currentLoaded = {}

window.loadSetting = (file,key,value) => {
$("#settings-inner").empty()
if(typeof value == undefined) return M.toast({html:`key "${key}" does not exist in ${file}`})

currentLoaded = {
    file:file,
    key:key,
    value:value,
}


    append = `
    <table>
    <thead>
      <tr>
          <th>key</th>
          <th>value</th>
      </tr>
    </thead>
    
    <tbody>
      <tr>
        <td>${key}</td>
        <td><textarea key="${key}" onChange="window.hasChanged(this,'${value}')" style="color:white;">${value}</textarea></td>
      </tr>
    </tbody>
    </table>
    `





$("#settings-inner").append(append)
}


window.cancel = () => {
    if(!currentLoaded) M.toast({html:`tried to cancel, went not right`})
    window.loadSetting(currentLoaded.file,currentLoaded.key,currentLoaded.value)
    $("#settings-inner").effect( "shake");
    $("#haschanged").slideUp("slow")
}




window.save = () => {
    try {
        let nested = currentLoaded.key.split("/")
        let evalstr = `jsonfiles['${currentLoaded.file}']${nested.map(n=>n=`['${n}']`).join("")} = '${latestValue}'`
        eval(evalstr)
        fs.writeFileSync(`./modules/settings/${currentLoaded.file}`,JSON.stringify(jsonfiles[currentLoaded.file]))
        $("#haschanged").slideUp("slow")
    } catch(err) {
        M.toast({html:err})
    }
}

let latestValue;
window.hasChanged = (that,standardValue) => {
let value = $(that).val()
latestValue = value
if(value == standardValue) {
    $("#haschanged").slideUp("slow")
    
} else {
    $("#haschanged").slideDown("fast")
}
}

$( document ).ready(function() {
    loadSettings()
});
