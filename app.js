let conf = require("../conf.json")
let current_user;
let ipc = require("electron").ipcRenderer


$(document).ready(function(){
    for(let _user of conf.users) {
        $('.carousel').append(`<a class="carousel-item"><img src="${_user.icon}"></a>`)   
    }
    $('.carousel').carousel({
        onCycleTo: (ele) => {
            current_user = ele
        }
    });
  });

  ipc.on("loginres",(e,obj) => {
      console.log(obj.msg)
      M.toast({html:obj.msg})
  })

  window.login = () => {
     let user = conf["users"][$(current_user).index()]
     ipc.send("login",user.token)
  }









